#!/usr/bin/ruby

 ###############################################################################
 #  Copyright 2006 Ian McIntosh <ian@openanswers.org>
 #
 #  This program is free software; you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation; either version 2 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Library General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with this program; if not, write to the Free Software
 #  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 ###############################################################################

Dir.chdir(File.dirname(__FILE__))	# So that this file can be run from anywhere
$LOAD_PATH << './utils'

# removed while we figure out why some users have problems with glade and safe mode $SAFE = 2	 # http://www.rubycentral.com/book/taint.html

###################################################################
# Constants
###################################################################
UNIT_PIXELS, UNIT_CENTIMETERS, UNIT_INCHES, UNIT_PICAS, UNIT_POINTS, UNIT_PERCENTAGE = (0..5).to_a
UNIT_LAST = UNIT_PERCENTAGE

APP_NAME			= 'GNOME Screen Ruler'
APP_COPYRIGHT	= "Copyright (c) #{Time.now.year} Ian McIntosh"
APP_AUTHORS 	= ['Ian McIntosh <ian@openanswers.org>']
APP_VERSION		= 0.80

DEFAULT_PPI		= 72.0

GCONF_PPI_PATH= '/desktop/gnome/font_rendering/dpi'
GCONF_ROOT		= '/apps/gruler'

###################################################################
# Includes
###################################################################
puts 'Loading libraries...'

require 'addons_ruby'									# for multi-file 'require'
require 'gtk2', 'libglade2', 'gconf2', 'addons_gtk', 'ruler_window', 'preferences_window'

###################################################################
# Main
###################################################################
Gtk.init

puts 'Connecting to GConf...'
	gconf = GConf::Client.default
	$ppi = gconf[GCONF_PPI_PATH] || DEFAULT_PPI
	gconf.set_root(GCONF_ROOT)

puts 'Creating windows...'
	$preferences_window = PreferencesWindow.new
	$ruler_window = RulerWindow.new
	$ruler_popup_menu = RulerPopupMenu.new

puts 'Reading settings...'
	$preferences_window.read_settings(gconf)
	$ruler_window.read_settings(gconf)
	$ruler_popup_menu.read_settings(gconf)

puts 'Presenting ruler...'
	$ruler_window.present

begin
	Gtk.main
ensure
	puts 'Shutting down...'
	[$ruler_window, $ruler_popup_menu, $preferences_window].each { |win| win.hide }				# feels snappy
	[$ruler_window, $ruler_popup_menu, $preferences_window].each { |win| win.write_settings(gconf) }
end
