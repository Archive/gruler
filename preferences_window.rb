 ###############################################################################
 #  Copyright 2006 Ian McIntosh <ian@openanswers.org>
 #
 #  This program is free software; you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation; either version 2 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Library General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with this program; if not, write to the Free Software
 #  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 ###############################################################################

require 'glade_window'

class PreferencesWindow < GladeWindow
	DEFAULT_FOREGROUND_COLOR = '#202020'
	DEFAULT_BACKGROUND_COLOR = '#E6E36B'
	
	def initialize
		super('preferences_window')

		# GTK signal handlers
		@window.signal_connect('delete_event') { hide }
	end

	def foreground_color ; return @foreground_color_button.color ; end
	def background_color ; return @background_color_button.color ; end
	def font ; return @text_fontbutton.font_name ; end
	def watch_mouse? ; return @watch_mouse_checkbutton.active? ; end
	def watch_mouse=(value) ; @watch_mouse_checkbutton.active = value ; end

	###################################################################
	# Settings
	###################################################################
	def read_settings(settings)
		@foreground_color_button.color = Gdk::Color.parse(settings['foreground_color'] || DEFAULT_FOREGROUND_COLOR)
		@background_color_button.color = Gdk::Color.parse(settings['background_color'] || DEFAULT_BACKGROUND_COLOR)
		@text_fontbutton.font_name = settings['font'] if settings['font']
	end

	def write_settings(settings)
		settings['foreground_color'] = self.foreground_color.to_hex
		settings['background_color'] = self.background_color.to_hex
		settings['font'] = self.font
	end

private

	###################################################################
	# Signal Handlers
	###################################################################
	def on_style_changed
		$ruler_window.redraw
	end

	def on_close_clicked
		hide
	end
end
