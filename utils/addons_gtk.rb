 ###############################################################################
 #  Copyright 2006 Ian McIntosh <ian@openanswers.org>
 #
 #  This program is free software; you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation; either version 2 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Library General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with this program; if not, write to the Free Software
 #  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 ###############################################################################

# extend/simplify/standardize some GTK objects

#################################
# GTK classes
#################################

class Gtk::Window
	def toggle_visibility
		if active?		# is this window the current top level window on the current desktop?
			hide
		else
			present		 # show / move to current desktop / bring to top
		end
	end
end

class Gdk::Rectangle
	def grow(m)
		return Gdk::Rectangle.new(self.x - m, self.y - m, self.width + m*2, self.height + m*2)
	end

	def shrink(m)
		grow(-m)
	end
	
	def contains(x, y)
		return false if x < self.x
		return false if y < self.y
		return false if x > self.x + self.width
		return false if y > self.y + self.height
		return true
	end

	def center
		return [self.x + (self.width/2), self.y + (self.height/2)]
	end
end

class Gdk::Color
	def to_hex		# use Gdk::Color.parse to parse
		sprintf('#%x%x%x', red, green, blue)
	end
end

#################################
# other
#################################

class Cairo::Context
	def show_pango_layout_centered(pangolayout, x, y)
		w,h = pangolayout.pixel_size
		move_to(x - (w / 2), y - (h / 2))
		show_pango_layout(pangolayout)
	end
end

class Pango::Layout
	def set_font_from_string(str)
		set_font_description(Pango::FontDescription.new(str))
	end
end

class GConf::Client
	def set_root(root)
		@root = root
	end
	
	def full_key_name(key)
		return @root + '/' + key if @root
		return key
	end

	def [](key)
		get(full_key_name(key))
	end
	
	def []=(key, value)
		set(full_key_name(key), value)
	end
end
